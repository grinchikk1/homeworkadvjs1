"use strict";

class Employee {
  constructor(name, age, salary) {
    this._name = name;
    this._age = age;
    this._salary = salary;
  }

  // гетери та сеттери
  get name() {
    return this._name;
  }

  set name(name) {
    this._name = name;
  }

  get age() {
    return this._age;
  }

  set age(age) {
    this._age = age;
  }

  get salary() {
    return this._salary;
  }

  set salary(salary) {
    this._salary = salary;
  }
}

class Programmer extends Employee {
  constructor(name, age, salary, lang) {
    super(name, age, salary);
    this._lang = lang;
  }

  // перезапис гетера для salary
  get salary() {
    return super.salary * 3;
  }

  // гетер та сеттер для lang
  get lang() {
    return this._lang;
  }

  set lang(lang) {
    this._lang = lang;
  }
}

// створення екземплярів Programmer та їх виведення у консоль
const d = new Programmer("Dmytro", 32, 18000, ["Dart", "JavaScript"]);
const a = new Programmer("Alisa", 25, 24000, ["Java", "C#", "C++"]);
const k = new Programmer("Kostya", 44, 20000, ["Python", "Swift"]);

console.log(d.name, d.age, d.salary, d.lang);
console.log(a.name, a.age, a.salary, a.lang);
console.log(k.name, k.age, k.salary, k.lang);
